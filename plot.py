import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import statistics
import refine
from matplotlib.widgets import Button
import pandas as pd


def find_center(vertex1, vertex2):
    center = ((vertex1[0]+vertex2[0])/2, (vertex1[1]+vertex2[1])/2)
    return center


def f2hex(f2rgb, f):
    rgb = f2rgb.to_rgba(f)[:3]
    return '#%02x%02x%02x' % tuple([int(255*fc) for fc in rgb])


def plot(virtualElements, x, outerDofNumbers, unselectedTriangles, refineCount, f, diameter, outerGlobalDofs, outerGlobalDofs2):
    print("REFINECOUNT:")
    print(refineCount)
    norm = colors.Normalize(vmin=min([0,min(x)]), vmax=max(x))
    f2rgb = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap('viridis'))
    x = x.tolist()
    result = []
    verticesSorted = []
    for i in range(len(x) + len(outerDofNumbers)):
        for v in virtualElements:
            for j in range(v.n):
                if v.globalDoFs[j] == i:
                    verticesSorted.append(v.vertices[j])
                    break
            if v.globalDoFs[j] == i:
                break
    # virtualElements = list(set(virtualElements))
    print("Max-Norm")
    print(max(x))

    outerDofNumbers = list(set(outerDofNumbers))
    zaehler = 0
    for i in range(len(x) + len(outerDofNumbers)):
        if i in outerDofNumbers:
            result.append(0)
        else:
            result.append(x[zaehler])
            zaehler += 1
    # virtualElements = list(set(virtualElements))

    for element in virtualElements:
        if element.n != 6:
            vertices = element.vertices
            colours2 = [result[z] for z in element.globalDoFs]
            colour_center = statistics.mean(colours2)
            centroid = element.centroid
            for i in range(3):
                a = find_center(vertices[i], vertices[(i + 1) % 3])
                b = find_center(vertices[i], vertices[(i - 1) % 3])
                x2 = find_center(a, vertices[i])
                y = find_center(b, vertices[i])
                t1_x = [x2[0], y[0], vertices[i][0]]
                t1_y = [x2[1], y[1], vertices[i][1]]
                t2_x = [x2[0], y[0], b[0], a[0]]
                t2_y = [x2[1], y[1], b[1], a[1]]
                t3_x = [centroid[0], a[0], b[0]]
                t3_y = [centroid[1], a[1], b[1]]
                plt.fill(t1_x, t1_y, f2hex(f2rgb, colours2[i]))
                plt.fill(t2_x, t2_y, f2hex(f2rgb, (colours2[i] + colour_center) / 2))
                plt.fill(t3_x, t3_y, f2hex(f2rgb, colour_center))
        else:
            vertices = element.vertices
            colours = [result[z] for z in element.globalDoFs]
            colour_center = statistics.mean(colours)

            centroid = element.centroid
            for i in range(6):
                h1 = find_center(vertices[i], vertices[(i+1) % 6])
                h2 = find_center(vertices[i], vertices[(i-1) % 6])
                v1 = find_center(h1, centroid)
                v2 = find_center(h2,centroid)
                t1_x = [vertices[i][0], h1[0], h2[0]]
                t1_y = [vertices[i][1], h1[1], h2[1]]
                t2_x = [h1[0], v1[0], v2[0], h2[0]]
                t2_y = [h1[1], v1[1], v2[1], h2[1]]
                t3_x = [v1[0], v2[0], centroid[0]]
                t3_y = [v1[1], v2[1], centroid[1]]
                plt.fill(t1_x, t1_y, f2hex(f2rgb, colours[i]))
                plt.fill(t2_x, t2_y, f2hex(f2rgb, (colours[i]+colour_center)/2))
                plt.fill(t3_x, t3_y, f2hex(f2rgb, colour_center))
    """for index, rows in unselectedTriangles.iterrows():
        verticesX = []
        verticesY = []
        for x in rows["vertices"]:
            verticesX.append(x[0])
            verticesY.append(x[1])
        plt.fill(verticesX, verticesY, "black")
    """
    plt.gca().invert_yaxis()
    plt.colorbar(f2rgb)
    plt.show()

    plt.close()

    if (refineCount > 0):
        refine.refine(virtualElements, f, diameter, unselectedTriangles, refineCount, outerGlobalDofs, outerGlobalDofs2)
        refineCount -= 1



