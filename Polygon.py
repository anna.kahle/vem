# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 15:37:52 2021

@author: Anna Kahle
"""

import numpy as np


class Polygon:
    """
    class for simple polygons to create its stiffnessMatrix

    Attributes:
    -----------
    vertices : list
        list of tuples of the positions for each vertex
    globalDoFs : list
        list to map the local doFs to the global ones
    stiffnessMatrix : np.array
        local stiffness matrix
    centroid : tuple
        position of the centroid of the polygon
    diameter : int
        diameter of the polygon
    a : int
        area of the polygon
    m : list
        list of function defining the basis polnomials for k=1

    Methods:
    --------
    createStiffnessMatrix():
        to create lokal stiffness matrix
    """

    def __init__(self, vertices, globalDoFs, centroid, diameter, a):
        self.lokalStiffnessMatrix = 0
        self.normalVectors = []
        self.sideLength = []
        self.vertices = vertices
        self.globalDoFs = globalDoFs
        self.centroid = centroid
        self.diameter = diameter
        self.n = len(vertices)
        # calculate edge length and normal vectors for all edges
        for i in range(self.n-1):
            self.sideLength.append(np.sqrt((self.vertices[i][0]-self.vertices[i+1][0])**2+(self.vertices[i][1]-self.vertices[i+1][1])**2))
            # TEST! EVTL WIEDER RÜCKGÄNGIG
            temp = (self.vertices[i+1][0] - self.vertices[i][0], self.vertices[i+1][1] - self.vertices[i][1])
            # temp = (self.vertices[i][0]-self.vertices[i+1][0], self.vertices[i][1]-self.vertices[i+1][1])
            normalV = (-temp[1], temp[0])
            if(temp[0] != 0 and temp[1] != 0):
                norm = np.sqrt(temp[1] ** 2 + temp[0] ** 2)
            elif(temp[0] != 0):
                norm = abs(temp[0])
            elif (temp[1] != 0):
                norm = abs(temp[1])
            normalV = (normalV[0]/norm, normalV[1]/norm)
            self.normalVectors.append(normalV)
        self.sideLength.append(np.sqrt((self.vertices[0][0]-self.vertices[self.n-1][0])**2+(self.vertices[0][1]-self.vertices[self.n-1][1])**2))
        temp = (-(self.vertices[self.n-1][0]-self.vertices[0][0]), -(self.vertices[self.n-1][1]-self.vertices[0][1]))
        normalV = (-temp[1], temp[0])
        if (temp[0] != 0 and temp[1] != 0):
            norm = np.sqrt(temp[1] ** 2 + temp[0] ** 2)
        elif (temp[0] != 0):
            norm = abs(temp[0])
        elif (temp[1] != 0):
            norm = abs(temp[1])
        normalV = (normalV[0] / norm, normalV[1] / norm)
        self.normalVectors.append(normalV)
        self.a = a
        m1 = lambda x: 1
        m2 = lambda x: (x[0]-centroid[0])/diameter
        m3 = lambda x: (x[1]-centroid[1])/diameter
        self.m = [m1, m2, m3]

    def createStiffnessMatrix(self):
        """
        create local stiffnessMatrix by computing B, D, G and multiplying them
        """
        B = self.createB()
        D = self.createD()
        G = self.createG()
        invG = np.linalg.inv(G)
        PStar = invG @ B
        P = D @ PStar
        GTilde = np.copy(G)
        # we only look at k=1, hence we only have 3 monomials
        GTilde[0] = [0, 0, 0]
        self.lokalStiffnessMatrix = np.add(np.transpose(PStar) @ GTilde @ PStar,
                                      np.transpose(np.identity(P.shape[0])-P) @ (np.identity(P.shape[0])-P))


    def createB(self):
        b = np.zeros((3, self.n))
        # since k=1 the entry i of the first row is the middle value
        # of phi_i on each vertix (1 on one, 0 elsewhere)
        b[0] = [1/self.n for x in self.vertices]
        b[1] = [0.5 * 1/self.diameter * (self.sideLength[i] * self.normalVectors[i][0]
                + self.sideLength[(i-1) % self.n] * self.normalVectors[(i-1) % self.n][0]) for
                i in range(self.n)]
        b[2] = [0.5 * 1 / self.diameter * (
                    self.sideLength[i] * self.normalVectors[i][1] +
                    + self.sideLength[(i-1) % self.n] * self.normalVectors[(i-1) % self.n][1])
                for i in range(self.n)]
        return b

    def createD(self):
        d = np.zeros((self.n, 3))
        for i in range(0, self.n):
            d[i][0] = self.m[0](self.vertices[i])
            d[i][1] = self.m[1](self.vertices[i])
            d[i][2] = self.m[2](self.vertices[i])
        return d

    def createG(self):
        g = np.zeros((3, 3))
        g[0][0] = 1/self.n * sum([self.m[0](self.vertices[i]) for i in range(len(self.vertices))])
        g[0][1] = 1/self.n * sum([self.m[1](self.vertices[i]) for i in range(len(self.vertices))])
        g[0][2] = 1/self.n * sum([self.m[2](self.vertices[i]) for i in range(len(self.vertices))])
        # other entries are 0 because of the scalar product
        # easy because for k=1 gradient only contains constants:
        # integrate 1/4 over polygon
        g[1][1] = (1/self.diameter)**2 * self.a
        g[2][2] = (1/self.diameter)**2 * self.a
        return g
