import mesh
import plot
import pandas as pd
import numpy as np

def refine(virtualElements, f, diameter, unselectedTrianglesOld, refineCount, outerGlobalDofs, outerGlobalDofs2):
    diameter = diameter / 2
    centroids = []
    vertices = []
    unselectedVertices = []
    unselectedCentroids = []
    for index, rows in unselectedTrianglesOld.iterrows():
        v = rows["vertices"]
        c = rows["centroid"]
        x = [v[0], (0.5 * (v[0][0] + v[1][0]), 0.5 * (v[0][1] + v[1][1])),
             (0.5 * (v[0][0] + v[2][0]), 0.5 * (v[0][1] + v[2][1]))]
        unselectedVertices.append(x)
        unselectedCentroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

        x = [v[1], (0.5 * (v[1][0] + v[2][0]), 0.5 * (v[1][1] + v[2][1])),
             (0.5 * (v[1][0] + v[0][0]), 0.5 * (v[1][1] + v[0][1]))]
        unselectedVertices.append(x)
        unselectedCentroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

        x = [v[2], (0.5 * (v[0][0] + v[2][0]), 0.5 * (v[0][1] + v[2][1])),
             (0.5 * (v[2][0] + v[1][0]), 0.5 * (v[2][1] + v[1][1]))]
        unselectedVertices.append(x)
        unselectedCentroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

        #gedreht
        x = [(0.5 * (v[0][0] + v[1][0]), 0.5 * (v[0][1] + v[1][1])),
             (0.5 * (v[2][0] + v[1][0]), 0.5 * (v[2][1] + v[1][1])),
             (0.5 * (v[0][0] + v[2][0]), 0.5 * (v[0][1] + v[2][1])),]
        unselectedVertices.append(x)
        unselectedCentroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

    unselectedTriangles = pd.DataFrame(list(zip(unselectedCentroids, unselectedVertices)),
                                columns=['centroid', 'vertices'])

    virtualElements = list(set(virtualElements))
    vertices = []

    for element in virtualElements:
        v = element.vertices
        c = element.centroid
        # HIER WIRD DAS SECHSECK REFINED
        if element.n == 6:
            verticesHexagons = refineHexagon(element)
            vertices = vertices + verticesHexagons
            for verticesHexagon in verticesHexagons:
                center = findCenter(verticesHexagon)
                centroids.append(center)
            """
            centroidsTriangle.append((1 / 3 * (c[0] + v[0][0] + v[5][0]),
                                      1 / 3 * (c[1] + v[0][1] + v[5][1])))
            verticesTriangle.append([c, v[5], v[0]])

            for k in range(5):
                centroidsTriangle.append((1 / 3 * (c[0] + v[k][0] + v[k + 1][0]),
                                          1 / 3 * (c[1] + v[k][1] + v[k + 1][1])))
                verticesTriangle.append([c, v[k], v[k + 1]])
            """
        else:
            x = [v[0], (0.5*(v[0][0]+v[1][0]), 0.5*(v[0][1]+v[1][1])), (0.5*(v[0][0]+v[2][0]), 0.5*(v[0][1]+v[2][1]))]
            vertices.append(x)
            centroids.append((1/3*(x[0][0]+x[1][0]+x[2][0]), 1/3*(x[0][1]+x[1][1]+x[2][1])))

            x = [v[1], (0.5 * (v[1][0] + v[2][0]), 0.5 * (v[1][1] + v[2][1])),
                 (0.5 * (v[1][0] + v[0][0]), 0.5 * (v[1][1] + v[0][1]))]
            vertices.append(x)
            centroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

            x = [v[2], (0.5 * (v[0][0] + v[2][0]), 0.5 * (v[0][1] + v[2][1])),
                 (0.5 * (v[2][0] + v[1][0]), 0.5 * (v[2][1] + v[1][1]))]
            vertices.append(x)
            centroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

            x = [(0.5 * (v[0][0] + v[1][0]), 0.5 * (v[0][1] + v[1][1])), (0.5 * (v[2][0] + v[1][0]), 0.5 * (v[2][1] + v[1][1])),
                 (0.5 * (v[0][0] + v[2][0]), 0.5 * (v[0][1] + v[2][1]))]
            vertices.append(x)
            centroids.append((1 / 3 * (x[0][0] + x[1][0] + x[2][0]), 1 / 3 * (x[0][1] + x[1][1] + x[2][1])))

    selected = pd.DataFrame(list(zip(centroids, vertices)),
                                     columns=['centroid', 'vertices'])

    globalDofs, outerGlobalDofs, virtualElements, globalStiffnessMatrix = mesh.globalDofs(selected, diameter,
                                                                                                  unselectedTriangles,
                                                                                                  outerGlobalDofs)
    f = lambda x: 1
    x, outerDofNumbers = mesh.solve(globalDofs, outerGlobalDofs, virtualElements, f, globalStiffnessMatrix,
                                            diameter)
    plot.plot(virtualElements, x, outerDofNumbers, unselectedTriangles, refineCount-1, f, diameter, outerGlobalDofs, outerGlobalDofs2)

#(0.5 * (v[a][0] + v[b][0]), 0.5 * (v[a][1] + v[b][1]))

def refineHexagon(element):
    v = element.vertices
    c = element.centroid
    centroids = []
    vertices = []
    hex1 = [v[0], (0.5 * (v[0][0] + v[1][0]), 0.5 * (v[0][1] + v[1][1])), (0.5 * (c[0] + v[1][0]), 0.5 * (c[1] + v[1][1])), c,
            (0.5 * (c[0] + v[5][0]), 0.5 * (c[1] + v[5][1])), (0.5 * (v[5][0] + v[0][0]), 0.5 * (v[5][1] + v[0][1]))]
    vertices.append(hex1)
    hex2 = [(0.5 * (v[1][0] + c[0]), 0.5 * (v[1][1] + c[1])), (0.5 * (v[1][0] + v[2][0]), 0.5 * (v[1][1] + v[2][1])), v[2],
            (0.5 * (v[2][0] + v[3][0]), 0.5 * (v[2][1] + v[3][1])), (0.5 * (c[0] + v[3][0]), 0.5 * (c[1] + v[3][1])), c]
    vertices.append(hex2)
    hex3 = [(0.5 * (c[0] + v[5][0]), 0.5 * (c[1] + v[5][1])), c, (0.5 * (c[0] + v[3][0]), 0.5 * (c[1] + v[3][1])),
            (0.5 * (v[3][0] + v[4][0]), 0.5 * (v[3][1] + v[4][1])), v[4], (0.5 * (v[4][0] + v[5][0]), 0.5 * (v[4][1] + v[5][1]))]
    vertices.append(hex3)
    tria1 = [(0.5 * (v[0][0] + v[1][0]), 0.5 * (v[0][1] + v[1][1])), v[1], (0.5 * (c[0] + v[1][0]), 0.5 * (c[1] + v[1][1]))]
    vertices.append(tria1)
    tria2 = [v[1], (0.5 * (v[1][0] + v[2][0]), 0.5 * (v[1][1] + v[2][1])), (0.5 * (c[0] + v[1][0]), 0.5 * (c[1] + v[1][1]))]
    vertices.append(tria2)
    tria3 = [(0.5 * (c[0] + v[3][0]), 0.5 * (c[1] + v[3][1])), (0.5 * (v[2][0] + v[3][0]), 0.5 * (v[2][1] + v[3][1])), v[3]]
    vertices.append(tria3)
    tria4 = [v[3], (0.5 * (v[3][0] + v[4][0]), 0.5 * (v[3][1] + v[4][1])), (0.5 * (c[0] + v[3][0]), 0.5 * (c[1] + v[3][1]))]
    vertices.append(tria4)
    tria5 = [v[5], (0.5 * (c[0] + v[5][0]), 0.5 * (c[1] + v[5][1])), (0.5 * (v[4][0] + v[5][0]), 0.5 * (v[4][1] + v[5][1]))]
    vertices.append(tria5)
    tria6 = [v[5], (0.5 * (v[0][0] + v[5][0]), 0.5 * (v[0][1] + v[5][1])), (0.5 * (c[0] + v[5][0]), 0.5 * (c[1] + v[5][1]))]
    vertices.append(tria6)
    return vertices
def findCenter(vertices):
    c0 = 0
    c1 = 0
    for x in vertices:
        c0 += x[0]
        c1 += x[1]
    c0 = c0 / len(vertices)
    c1 = c1 / len(vertices)
    return (c0, c1)
