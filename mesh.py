# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 11:00:50 2021

@author: Anna Kahle
"""
import numpy as np
import pandas as pd
from Polygon import Polygon


def hexagonGrid(n, diameter):
    """
    create centroids and vertices for all hexagons
    """
    centroid = [[0 for x in range(n)] for y in range(n)]
    notClickable = [[0 for x in range(n)] for y in range(n)]
    vertices = []
    # create first row of hexagons, all other lines will depend on the
    # rows before
    for i in range(n):
        centroid[i][0] = (diameter/2+i*diameter*np.sqrt(3)/2, diameter)
        vertices.append(verticesHexagon(centroid[i][0], diameter))
        notClickable[i][0] = 1
    # create all other hexagons
    for j in range(1, n):
        for i in range(0, n):
            if(j % 2 == 1):
                centroid[i][j] = (centroid[i][j-1][0]
                                  + 0.5*np.sqrt(3)/2*diameter,
                                  centroid[i][j-1][1]+0.75*diameter)
            else:
                centroid[i][j] = (centroid[i][j-2][0],
                                  centroid[i][j-2][1]+1.5*diameter)
            if(j==n-1 or i==0 or i==n-1):
                notClickable[i][j] = 1
            vertices.append(verticesHexagon(centroid[i][j], diameter))
    # we need a list of outer global dofs for the dirichlet boundary
    # conditions
    outerGlobalDofs = []
    outerGlobalDofs2 = []
    hight = diameter/2
    outerGlobalDofs.append((centroid[0][0][0]-np.sqrt(3)/2*hight, centroid[0][0][1]+1/2*hight))
    outerGlobalDofs.append((centroid[n-1][0][0]+np.sqrt(3)/2*hight, centroid[n-1][0][1]+1/2*hight))
    for i in range(n):
        outerGlobalDofs2.append((centroid[i][0][0]+np.sqrt(3)/2*hight, centroid[i][0][1]-1/2*hight))
        outerGlobalDofs2.append((centroid[i][0][0], centroid[i][0][1]-hight))
        outerGlobalDofs2.append((centroid[i][0][0]-np.sqrt(3)/2*hight, centroid[i][0][1]-1/2*hight))
    for j in range(1,n-1):
        outerGlobalDofs2.append((centroid[0][j][0]-np.sqrt(3)/2*hight, centroid[0][j][1]+1/2*hight))
        outerGlobalDofs2.append((centroid[0][j][0]-np.sqrt(3)/2*hight, centroid[0][j][1]-1/2*hight))
        outerGlobalDofs2.append((centroid[n-1][j][0]+np.sqrt(3)/2*hight, centroid[n-1][j][1]+1/2*hight))
        outerGlobalDofs2.append((centroid[n-1][j][0]+np.sqrt(3)/2*hight, centroid[n-1][j][1]-1/2*hight))
    outerGlobalDofs2.append((centroid[0][n-1][0]-np.sqrt(3)/2*hight, centroid[0][n-1][1]-1/2*hight))
    outerGlobalDofs2.append((centroid[n-1][n-1][0]+np.sqrt(3)/2*hight, centroid[n-1][n-1][1]-1/2*hight))
    for i in range(n):
        outerGlobalDofs2.append((centroid[i][n-1][0]-np.sqrt(3)/2*hight, centroid[i][n-1][1]+1/2*hight))
        outerGlobalDofs2.append((centroid[i][n-1][0], centroid[i][n-1][1]+hight))
        outerGlobalDofs2.append((centroid[i][n-1][0]+np.sqrt(3)/2*hight, centroid[i][n-1][1]+1/2*hight))
    outerGlobalDofs2 = list(set(outerGlobalDofs2))
    outerGlobalDofs.extend(outerGlobalDofs2)

    # flatten list of centroids
    notClickables = []
    centroids = []
    for i in range(n):
        for j in range(n):
            centroids.append(centroid[j][i])
            notClickables.append(notClickable[j][i])

    # dataframe of hexagons
    hexagons = pd.DataFrame(list(zip(centroids, vertices, notClickables)),
                            columns=['centroid', 'vertices', 'not clickable'])
    return hexagons, outerGlobalDofs, outerGlobalDofs2


def triangleGrid(hexagons, n, diameter):
    """
    create centroids and vertices for all triangles
    """
    centroidsTriangle = []
    verticesTriangle = []
    notclickable = []
    for index, row in hexagons.iterrows():
        notclickable.append(row["not clickable"])
        if(row["selected"] == 0):
            v = row["vertices"]
            c = row["centroid"]
            centroidsTriangle.append((1/3*(c[0]+v[0][0]+v[5][0]),
                                      1/3*(c[1]+v[0][1]+v[5][1])))
            verticesTriangle.append([c, v[5], v[0]])
            for k in range(5):
                notclickable.append(row["not clickable"])
                centroidsTriangle.append((1/3*(c[0]+v[k][0]+v[k+1][0]),
                                          1/3*(c[1]+v[k][1]+v[k+1][1])))
                verticesTriangle.append([c, v[k], v[k+1]])
    triangles = pd.DataFrame(list(zip(centroidsTriangle, verticesTriangle, notclickable)),
                             columns=['centroid', 'vertices', 'not clickable'])
    return triangles


def verticesHexagon(centroid, diameter):
    """
    calculates the vertices for a regular hexagon given its centroid and
    diameter
    """
    hight = diameter/2
    x1 = (centroid[0], centroid[1]+hight)
    x2 = (centroid[0]+np.sqrt(3)/2*hight, centroid[1]+1/2*hight)
    x3 = (centroid[0]+np.sqrt(3)/2*hight, centroid[1]-1/2*hight)
    x4 = (centroid[0], centroid[1]-hight)
    x5 = (centroid[0]-np.sqrt(3)/2*hight, centroid[1]-1/2*hight)
    x6 = (centroid[0]-np.sqrt(3)/2*hight, centroid[1]+1/2*hight)
    return [x1, x2, x3, x4, x5, x6]


def globalDofs(polygons, diameter, unselected, outerGlobalDofs):
    """
    calculate global dofs, calls function to calculate global stiffness matrix
    """
    globalDofs = []
    listGlobalDofsElement = []
    numberOfGlobalDofs = 0
    for index, rows in polygons.iterrows():
        globalDofsElement = []
        for v in rows["vertices"]:
            found = 0
            for i in range(len(globalDofs)):
                # because of the sqare root the vertices do not match exactly
                if(abs(globalDofs[i][0]-v[0])+abs(globalDofs[i][1]-v[1])
                   < diameter/10000):
                    found = 1
                    globalDofsElement.append(i)
            if found == 0:
                globalDofsElement.append(len(globalDofs))
                globalDofs.append(v)
                numberOfGlobalDofs += 1
        listGlobalDofsElement.append(globalDofsElement)
    polygons["globaldofs"] = listGlobalDofsElement
    # we need a list of outer global dofs for the dirichlet boundary
    # conditions, temp stores all vertices which belong to selected hexagons/triangles, but also to unselected triangles
    temp = []
    for index, rowUnselected in unselected.iterrows():
        for v in rowUnselected["vertices"]:
            for w in globalDofs:
                if(abs(w[0]-v[0])+abs(w[1]-v[1])
                   < diameter/1000):
                    temp.append(v)

    # remove duplicates
    temp = list(set(temp))
    # combine with list of vertices which are at the border of the selectable grid
    outerGlobalDofs.extend(temp)
    virtualElements, globalStiffnessMatrix = vem(polygons, diameter, len(globalDofs))


    return globalDofs, outerGlobalDofs, virtualElements, globalStiffnessMatrix




def vem(polygons, diameter, numberGlobalDofs):
    """
    calculate global stiffness matrix
    """
    globalStiffnessMatrix = np.zeros((numberGlobalDofs, numberGlobalDofs))
    virtualElements = []
    for index, rows in polygons.iterrows():
        vertices = rows["vertices"]
        centroid = rows["centroid"]
        globalDofsElement = rows["globaldofs"]
        if(len(vertices) == 6):
            virtualElement = Polygon(vertices, globalDofsElement, centroid,
                                     diameter, 6/16*diameter**2*np.sqrt(3))
            virtualElements.append(virtualElement)
        else:
            diameterTr = np.sqrt((vertices[0][0]-vertices[1][0])**2+(vertices[0][1]-vertices[1][1])**2)
            virtualElement = Polygon(vertices, globalDofsElement, centroid,
                                     #WIE WIRD DER FLÄCHENINHALT TATSÄCHLICH BERECHNET???
                                     diameterTr, 1/4*diameterTr**2*np.sqrt(3))
        virtualElements.append(virtualElement)
        virtualElement.createStiffnessMatrix()
        C = np.zeros((numberGlobalDofs, len(vertices)))
        for i in range(len(vertices)):
            C[globalDofsElement[i]][i] = 1
        globalStiffnessMatrix += C @ virtualElement.lokalStiffnessMatrix @ np.transpose(C)
    return virtualElements, globalStiffnessMatrix


def solve(globalDofs, outerGlobalDofs, virtualElements, function, globalStiffnessMatrix, diameter):
    f = [0 for i in range(len(globalDofs))]
    outerDofNumbers = []
    globalStiffnessMatrix = np.array(globalStiffnessMatrix)
    virtualElements = list(set(virtualElements))
    # find all vertices which are on the edge and also all prepare the right side
    for i in range(len(globalDofs)):
        for v in virtualElements:
            inside = 1
            for j in range(v.n):
                if i == v.globalDoFs[j]:
                    # we only want to add something if we are not at the boundary
                    for outerVertex in outerGlobalDofs:
                        if(abs(outerVertex[0]-v.vertices[j][0])+abs(outerVertex[1]-v.vertices[j][1])
                                < diameter/1000):
                            outerDofNumbers.append(i)
                            # inside = 0
                        for vertex in v.vertices:
                            if (abs(outerVertex[0] - vertex[0]) + abs(outerVertex[1] - vertex[1])
                                    < diameter / 1000):
                                inside = 0
                    f[i] += v.a*1/v.n*1/v.n*sum([function(x) for x in v.vertices])
    f = np.array(f)
    f = np.delete(f, outerDofNumbers, 0)

    globalStiffnessMatrix = np.delete(globalStiffnessMatrix, outerDofNumbers, 0)
    globalStiffnessMatrix = np.delete(globalStiffnessMatrix, outerDofNumbers, 1)

    return np.linalg.solve(globalStiffnessMatrix, f), outerDofNumbers
