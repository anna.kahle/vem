# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 11:00:15 2021
Create gui for selecting hexagon and triangle grid on which
the Virtual Element Method should be used to calculate the stiffness
matrix for the Poisson equation with homogeneous boundary conditions
and k=1

@author: Anna Kahle
"""

import tkinter as tk
import mesh
import pandas as pd
import plot
import sys
import numpy as np


# diameter of hexagon, diameter of triangle is half of it
diameter = 50
f = lambda x: 1
refineCount = 2

def refine():
    """
    show and select triangles
    """
    # state: create and select triangles
    global state
    state = 1
    # forget all hexagons we do not want to evaluate
    unselected = hexagons[hexagons["selected"] == 0]
    # unselected = unselected[unselected['not clickable'] == 0]
    unselected2 = unselected[unselected['not clickable'] == 0]
    refineButton["state"] = "disable"
    evaluationButton["state"] = "normal"
    global triangles
    triangles = mesh.triangleGrid(unselected, n, diameter)
    plotTriangles()


def evaluation():
    """
    prepare for the calculation of the stiffness matrix
    """
    global state
    # state: create stiffness matrix
    state = 2
    evaluationButton["state"] = "disabled"
    selectedHexagons = hexagons[hexagons["selected"] == 1]
    selectedTriangles = triangles[triangles["selected"] == 1]
    selected = pd.concat([selectedHexagons, selectedTriangles])
    unselectedTriangles = triangles[triangles["selected"] == 0]
    global outerGlobalDofs
    globalDofs, outerGlobalDofs, virtualElements, globalStiffnessMatrix = mesh.globalDofs(selected, diameter, unselectedTriangles, outerGlobalDofs)
    x, outerDofNumbers= mesh.solve(globalDofs, outerGlobalDofs, virtualElements, f, globalStiffnessMatrix, diameter)
    root.destroy()
    plot.plot(virtualElements, x, outerDofNumbers, unselectedTriangles, refineCount, f, diameter, outerGlobalDofs, outerGlobalDofs2)



def plotHexagon():
    """
    plot all hexagons and make them clickable
    """
    hexagons["selected"] = [0 for i in range(n*n)]
    for index, row in hexagons.iterrows():
        vertices = row["vertices"]
        notClickable = row['not clickable']
        if(notClickable):
            hexagon = my_canvas.create_polygon(vertices[0][0], vertices[0][1],
                                               vertices[1][0], vertices[1][1],
                                               vertices[2][0], vertices[2][1],
                                               vertices[3][0], vertices[3][1],
                                               vertices[4][0], vertices[4][1],
                                               vertices[5][0], vertices[5][1],
                                               fill="grey", outline="black")
        else:
            hexagon = my_canvas.create_polygon(vertices[0][0], vertices[0][1],
                                               vertices[1][0], vertices[1][1],
                                               vertices[2][0], vertices[2][1],
                                               vertices[3][0], vertices[3][1],
                                               vertices[4][0], vertices[4][1],
                                               vertices[5][0], vertices[5][1],
                                               fill="light grey", outline="black")
        my_canvas.tag_bind(hexagon, "<Button-1>", onHexagonClick)
        my_canvas.tag_bind(hexagon, "<B1-Motion>", onHexagonClick)


def onHexagonClick(event):
    """
    if hexagon is clicked: fill hexagon and mark it in table
    only if hexagon mode
    """
    global hexagons
    if state == 0:
        minDiff = 10000000
        minIndex = 0
        for index, rows in hexagons.iterrows():
            centroid = rows["centroid"]
            vertices = rows["vertices"]
            diff = abs(event.x-centroid[0])+abs(event.y-centroid[1])
            if(diff < minDiff):
                minDiff = diff
                minIndex = index
        hexagon = hexagons.iloc[minIndex]
        vertices = hexagon["vertices"]
        if(not hexagon['not clickable']):
            my_canvas.create_polygon(vertices[0][0], vertices[0][1],
                                     vertices[1][0], vertices[1][1],
                                     vertices[2][0], vertices[2][1],
                                     vertices[3][0], vertices[3][1],
                                     vertices[4][0], vertices[4][1],
                                     vertices[5][0], vertices[5][1],
                                     fill="red", outline="black")
            hexagons.at[minIndex, "selected"] = 1


def plotTriangles():
    """
    plot all triangles and make them clickable
    """
    triangles["selected"] = [0 for i in range(len(triangles.index))]
    for index, row in triangles.iterrows():
        vertices = row["vertices"]
        if(row["not clickable"] == 0):
            hexagon = my_canvas.create_polygon(vertices[0][0], vertices[0][1],
                                               vertices[1][0], vertices[1][1],
                                               vertices[2][0], vertices[2][1],
                                               fill="light grey", outline="black")
            my_canvas.tag_bind(hexagon, "<Button-1>", onTriangleClick)
            my_canvas.tag_bind(hexagon, "<B1-Motion>", onTriangleClick)


def onTriangleClick(event):
    """
    if triangle is clicked: fill triangle and mark it in table
    only if triangle mode
    """
    if state == 1:
        minDiff = 10000000
        minIndex = 0
        for index, rows in triangles.iterrows():
            centroid = rows["centroid"]
            diff = (event.x-centroid[0])**2+(event.y-centroid[1])**2
            if(diff < minDiff):
                minDiff = diff
                minIndex = index
        vertices = triangles.iloc[minIndex]["vertices"]
        my_canvas.create_polygon(vertices[0][0], vertices[0][1],
                                 vertices[1][0], vertices[1][1],
                                 vertices[2][0], vertices[2][1],
                                 fill="red", outline="black")
        triangles.at[minIndex, "selected"] = 1

# state: create and select hexagons
state = 0

#file_path = 'randomfile.txt'
#sys.stdout = open(file_path, "w")

# create window
root = tk.Tk()
root.geometry("500x500")
my_canvas = tk.Canvas(root, width=400, height=400, bg="white")
my_canvas.pack(pady=20)

# number of hexagons per row and column
n = 400//diameter

hexagons, outerGlobalDofs, outerGlobalDofs2 = mesh.hexagonGrid(n, diameter)

plotHexagon()

hexagons2 = hexagons[hexagons['not clickable'] == 0]

triangles = 0
refineButton = tk.Button(root, text="Triangles", command=refine)
refineButton.pack()

evaluationButton = tk.Button(root, text="Evaluation", command=evaluation)
evaluationButton["state"] = "disable"
evaluationButton.pack()

root.mainloop()
