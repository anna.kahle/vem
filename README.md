
## Name
vem

## Description
This program lets user select a simple region in 2D to solve the Poisson equation in this region via the Virtual Element Method

## Usage
Execute gui.py. It is possible to change the diameter of the virtual elements at the top (preferably between 30 and 100). Also it is possible to change the function on the right side of the poisson equation and the number of refinements which should be made after the first calculation. The refinements will be calculated one after another as soon as the latest image is closed.

